'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _q = require('q');

var _q2 = _interopRequireDefault(_q);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _mkdirp = require('mkdirp');

var _mkdirp2 = _interopRequireDefault(_mkdirp);

var _handlebars = require('handlebars');

var _handlebars2 = _interopRequireDefault(_handlebars);

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _ncp = require('ncp');

var _ncp2 = _interopRequireDefault(_ncp);

var _cheerio = require('cheerio');

var _cheerio2 = _interopRequireDefault(_cheerio);

var _fileExists = require('file-exists');

var _fileExists2 = _interopRequireDefault(_fileExists);

var _childProcessPromise = require('child-process-promise');

var _easyimage = require('easyimage');

var _easyimage2 = _interopRequireDefault(_easyimage);

var _xml2js = require('xml2js');

var _xml2js2 = _interopRequireDefault(_xml2js);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

require('babel-polyfill');

_handlebars2.default.registerHelper('ifCond', function (v1, operator, v2, options) {

  switch (operator) {
    case '==':
      return v1 == v2 ? options.fn(this) : options.inverse(this);
    case '===':
      return v1 === v2 ? options.fn(this) : options.inverse(this);
    case '<':
      return v1 < v2 ? options.fn(this) : options.inverse(this);
    case '<=':
      return v1 <= v2 ? options.fn(this) : options.inverse(this);
    case '>':
      return v1 > v2 ? options.fn(this) : options.inverse(this);
    case '>=':
      return v1 >= v2 ? options.fn(this) : options.inverse(this);
    case '&&':
      return v1 && v2 ? options.fn(this) : options.inverse(this);
    case '||':
      return v1 || v2 ? options.fn(this) : options.inverse(this);
    default:
      return options.inverse(this);
  }
});

var ProsvConverter = function () {
  function ProsvConverter(bookObject, doNotUpdateResources) {
    _classCallCheck(this, ProsvConverter);

    if (!bookObject) throw new Error('Missing argument: bookObject');
    this._doNotUpdateResources = doNotUpdateResources ? true : false;
    this._bookObject = bookObject;
  }

  _createClass(ProsvConverter, [{
    key: 'convert',
    value: function () {
      var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(destDir) {
        var _this = this;

        var objects, objectsPath, pdfDir, docsDir, outline, additionals, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, o, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _loop, _iterator2, _step2, objectPath, templatePath, objectHtmlPath, iconUrl, gallery, outlineObject, contentTemplatePath, contentHtmlPath, indexTemplatePath, indexHtmlPath;

        return regeneratorRuntime.wrap(function _callee$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                objects = [];

                _mkdirp2.default.sync(destDir);
                _fs2.default.writeFileSync(_path2.default.join(destDir, 'index.json'), JSON.stringify(this._bookObject, null, 2));
                this._bookObject.contents.forEach(function (c) {
                  _this._linearize(c, objects);
                });
                objectsPath = _path2.default.join(destDir, './objects');
                _context2.next = 7;
                return this._copyBase(destDir);

              case 7:
                pdfDir = _path2.default.join(destDir, './pdf');
                //var indexPdfPath = path.join(pdfDir, 'index.prosvcontent.pdf');

                docsDir = _path2.default.join(pdfDir, './docs');

                _mkdirp2.default.sync(docsDir);
                _context2.next = 12;
                return this._downloadFile(this._bookObject.pdf.url, destDir, false, 'index.prosvcontent.pdf');

              case 12:
                //console.log('pdf2json');
                //await execFile('pdf2json', [indexPdfPath, '-q', '-enc', 'UTF-8', '-compress', path.join(docsDir, 'index.pdf.js')], {maxBuffer: 500 * 1024 * 1024});
                //console.log('mudraw');
                //await execFile('mudraw', ['-r120', '-o', path.join(docsDir, 'index.pdf_%d.png'), indexPdfPath]);
                //await execFile('mudraw', ['-r12', '-o', path.join(docsDir, 'index.pdf_%d_thumb.png'), indexPdfPath]);
                //await execFile('find', [docsDir, '-name', '*.png', '-exec', 'convert', '{}', '{}.jpg', '\;']);
                //await execFile('pdftk', [indexPdfPath, 'burst', 'output', path.join(docsDir, 'index_%1d.pdf'), 'compress']);
                //await execFile('rm', [indexPdfPath]);
                outline = [];
                additionals = null;
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context2.prev = 17;

                for (_iterator = objects[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                  o = _step.value;

                  if (o.scope && o.scope.additional && !o.tile) {
                    if (!additionals) additionals = [];
                    additionals.push({
                      title: o.title.value,
                      page: o.page.pdf
                    });
                  }
                }
                _context2.next = 25;
                break;

              case 21:
                _context2.prev = 21;
                _context2.t0 = _context2['catch'](17);
                _didIteratorError = true;
                _iteratorError = _context2.t0;

              case 25:
                _context2.prev = 25;
                _context2.prev = 26;

                if (!_iteratorNormalCompletion && _iterator.return) {
                  _iterator.return();
                }

              case 28:
                _context2.prev = 28;

                if (!_didIteratorError) {
                  _context2.next = 31;
                  break;
                }

                throw _iteratorError;

              case 31:
                return _context2.finish(28);

              case 32:
                return _context2.finish(25);

              case 33:
                _iteratorNormalCompletion2 = true;
                _didIteratorError2 = false;
                _iteratorError2 = undefined;
                _context2.prev = 36;
                _loop = regeneratorRuntime.mark(function _loop() {
                  var o, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, m;

                  return regeneratorRuntime.wrap(function _loop$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          o = _step2.value;

                          if (!(o.scope && o.scope.digital)) {
                            _context.next = 55;
                            break;
                          }

                          objectPath = _path2.default.join(objectsPath, o.id.toString());

                          _mkdirp2.default.sync(objectPath);
                          _context.next = 6;
                          return _this._relativize(o, objectPath);

                        case 6:
                          if (!o.media) {
                            _context.next = 33;
                            break;
                          }

                          _iteratorNormalCompletion3 = true;
                          _didIteratorError3 = false;
                          _iteratorError3 = undefined;
                          _context.prev = 10;
                          _iterator3 = o.media[Symbol.iterator]();

                        case 12:
                          if (_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done) {
                            _context.next = 19;
                            break;
                          }

                          m = _step3.value;
                          _context.next = 16;
                          return _this._relativize(m, objectPath);

                        case 16:
                          _iteratorNormalCompletion3 = true;
                          _context.next = 12;
                          break;

                        case 19:
                          _context.next = 25;
                          break;

                        case 21:
                          _context.prev = 21;
                          _context.t0 = _context['catch'](10);
                          _didIteratorError3 = true;
                          _iteratorError3 = _context.t0;

                        case 25:
                          _context.prev = 25;
                          _context.prev = 26;

                          if (!_iteratorNormalCompletion3 && _iterator3.return) {
                            _iterator3.return();
                          }

                        case 28:
                          _context.prev = 28;

                          if (!_didIteratorError3) {
                            _context.next = 31;
                            break;
                          }

                          throw _iteratorError3;

                        case 31:
                          return _context.finish(28);

                        case 32:
                          return _context.finish(25);

                        case 33:
                          if (o.type) {
                            _context.next = 35;
                            break;
                          }

                          throw new Error('Content type is empty. ObjectId: ' + o.id);

                        case 35:
                          templatePath = _path2.default.join(_path2.default.join(__dirname, 'templates/content'), o.type + '.html');
                          objectHtmlPath = _path2.default.join(objectPath, 'index.html');

                          o.additionals = additionals;
                          o.hasAdditionals = o.additionals || o.trainer || o.control ? true : false;

                          _this._fillTemplate(templatePath, o, objectHtmlPath);

                          if (!(o.control && o.control.url)) {
                            _context.next = 44;
                            break;
                          }

                          _context.next = 43;
                          return _this._downloadFile(o.control.url, objectPath, false, 'control.html');

                        case 43:
                          _this._processContent(objectPath + '/control.html');

                        case 44:
                          if (!(o.trainer && o.trainer.url)) {
                            _context.next = 48;
                            break;
                          }

                          _context.next = 47;
                          return _this._downloadFile(o.trainer.url, objectPath, false, 'trainer.html');

                        case 47:
                          _this._processContent(objectPath + '/trainer.html');

                        case 48:
                          iconUrl = null;

                          if (o.images) {
                            o.images.forEach(function (i) {
                              if (i.isMain) iconUrl = i.url;
                            });
                          }
                          gallery = o.images.map(function (img) {
                            return {
                              title: img.title.replace(/<(?:.|\n)*?>/gm, ''),
                              src: './objects/' + o.id.toString() + '/' + img.url
                            };
                          });
                          outlineObject = {
                            id: o.id,
                            title: o.title.value,
                            url: './objects/' + o.id.toString() + '/index.html',
                            page: o.page.pdf,
                            gallery: gallery,
                            level: o.level,
                            active: o.active
                          };

                          if (gallery && gallery.length > 0) o.gallery = gallery;
                          if (iconUrl) outlineObject.icon = './objects/' + o.id.toString() + '/' + iconUrl;
                          outline.push(outlineObject);

                        case 55:
                        case 'end':
                          return _context.stop();
                      }
                    }
                  }, _loop, _this, [[10, 21, 25, 33], [26,, 28, 32]]);
                });
                _iterator2 = objects[Symbol.iterator]();

              case 39:
                if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                  _context2.next = 44;
                  break;
                }

                return _context2.delegateYield(_loop(), 't1', 41);

              case 41:
                _iteratorNormalCompletion2 = true;
                _context2.next = 39;
                break;

              case 44:
                _context2.next = 50;
                break;

              case 46:
                _context2.prev = 46;
                _context2.t2 = _context2['catch'](36);
                _didIteratorError2 = true;
                _iteratorError2 = _context2.t2;

              case 50:
                _context2.prev = 50;
                _context2.prev = 51;

                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                  _iterator2.return();
                }

              case 53:
                _context2.prev = 53;

                if (!_didIteratorError2) {
                  _context2.next = 56;
                  break;
                }

                throw _iteratorError2;

              case 56:
                return _context2.finish(53);

              case 57:
                return _context2.finish(50);

              case 58:
                contentTemplatePath = _path2.default.join(__dirname, 'templates/index.prosvcontent.html');
                contentHtmlPath = _path2.default.join(destDir, 'index.prosvcontent.html');

                this._fillTemplate(contentTemplatePath, { outline: outline }, contentHtmlPath);
                indexTemplatePath = _path2.default.join(__dirname, 'templates/index.html');
                indexHtmlPath = _path2.default.join(destDir, 'index.html');

                this._fillTemplate(indexTemplatePath, this._bookObject, indexHtmlPath);

              case 64:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee, this, [[17, 21, 25, 33], [26,, 28, 32], [36, 46, 50, 58], [51,, 53, 57]]);
      }));

      function convert(_x) {
        return _ref.apply(this, arguments);
      }

      return convert;
    }()
  }, {
    key: '_processContent',
    value: function _processContent(filePath) {
      var templateData = _fs2.default.readFileSync(filePath, 'UTF-8');
      templateData = templateData.replace(/close\:\/\/window\.local/g, 'index.html');
      var $ = _cheerio2.default.load(templateData);
      $('html,body').attr('style', 'overflow-x: auto');
      _fs2.default.writeFileSync(filePath, $.html());
    }
  }, {
    key: '_fillTemplate',
    value: function _fillTemplate(templatePath, data, destPath) {
      try {
        _fs2.default.accessSync(templatePath, _fs2.default.F_OK);
      } catch (e) {
        throw new Error('Template "' + templatePath + '" not found');
      }
      var templateData = _fs2.default.readFileSync(templatePath, 'UTF-8');
      var template = _handlebars2.default.compile(templateData);
      var html = template(data);
      html = html.replace(/catalog\.prosv\.ru/g, 'prosv.biblio.rt.ru');
      _fs2.default.writeFileSync(destPath, html);
    }
  }, {
    key: '_copyBase',
    value: function () {
      var _ref2 = _asyncToGenerator(regeneratorRuntime.mark(function _callee2(destDir) {
        var def, baseDir;
        return regeneratorRuntime.wrap(function _callee2$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                def = _q2.default.defer();
                baseDir = _path2.default.join(__dirname, 'templates/base');

                _ncp2.default.ncp(baseDir, destDir, function (err) {
                  if (err) return def.reject(err);
                  def.resolve();
                });
                return _context3.abrupt('return', def.promise);

              case 4:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee2, this);
      }));

      function _copyBase(_x2) {
        return _ref2.apply(this, arguments);
      }

      return _copyBase;
    }()
  }, {
    key: '_relativize',
    value: function () {
      var _ref3 = _asyncToGenerator(regeneratorRuntime.mark(function _callee3(object, destDir) {
        var fieldsToRelativizeText, _iteratorNormalCompletion4, _didIteratorError4, _iteratorError4, _iterator4, _step4, field, fieldsToDownload, _iteratorNormalCompletion5, _didIteratorError5, _iteratorError5, _iterator5, _step5, _field, urls, _iteratorNormalCompletion6, _didIteratorError6, _iteratorError6, _iterator6, _step6, urlObject, downloadUrl;

        return regeneratorRuntime.wrap(function _callee3$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                fieldsToRelativizeText = ['text'];
                _iteratorNormalCompletion4 = true;
                _didIteratorError4 = false;
                _iteratorError4 = undefined;
                _context4.prev = 4;

                for (_iterator4 = fieldsToRelativizeText[Symbol.iterator](); !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                  field = _step4.value;

                  if (object[field] !== undefined && typeof object[field] === 'string') {
                    object[field] = this._relativizeText(object[field]);
                  }
                }
                _context4.next = 12;
                break;

              case 8:
                _context4.prev = 8;
                _context4.t0 = _context4['catch'](4);
                _didIteratorError4 = true;
                _iteratorError4 = _context4.t0;

              case 12:
                _context4.prev = 12;
                _context4.prev = 13;

                if (!_iteratorNormalCompletion4 && _iterator4.return) {
                  _iterator4.return();
                }

              case 15:
                _context4.prev = 15;

                if (!_didIteratorError4) {
                  _context4.next = 18;
                  break;
                }

                throw _iteratorError4;

              case 18:
                return _context4.finish(15);

              case 19:
                return _context4.finish(12);

              case 20:
                fieldsToDownload = ['images', 'textAttachments', 'icon', 'file'];
                _iteratorNormalCompletion5 = true;
                _didIteratorError5 = false;
                _iteratorError5 = undefined;
                _context4.prev = 24;
                _iterator5 = fieldsToDownload[Symbol.iterator]();

              case 26:
                if (_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done) {
                  _context4.next = 62;
                  break;
                }

                _field = _step5.value;

                if (!object[_field]) {
                  _context4.next = 59;
                  break;
                }

                urls = null;

                if (object[_field] instanceof Array) {
                  urls = object[_field];
                } else {
                  urls = [object[_field]];
                }
                _iteratorNormalCompletion6 = true;
                _didIteratorError6 = false;
                _iteratorError6 = undefined;
                _context4.prev = 34;
                _iterator6 = urls[Symbol.iterator]();

              case 36:
                if (_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done) {
                  _context4.next = 45;
                  break;
                }

                urlObject = _step6.value;
                downloadUrl = urlObject.url;

                urlObject.url = this._relativizeText(urlObject.url);
                _context4.next = 42;
                return this._downloadFile(downloadUrl, destDir, true, null, ['images', 'icon', 'textAttachments'].indexOf(_field) !== -1 && !downloadUrl.toLowerCase().endsWith('.svg') ? { width: 600, height: 600 } : null);

              case 42:
                _iteratorNormalCompletion6 = true;
                _context4.next = 36;
                break;

              case 45:
                _context4.next = 51;
                break;

              case 47:
                _context4.prev = 47;
                _context4.t1 = _context4['catch'](34);
                _didIteratorError6 = true;
                _iteratorError6 = _context4.t1;

              case 51:
                _context4.prev = 51;
                _context4.prev = 52;

                if (!_iteratorNormalCompletion6 && _iterator6.return) {
                  _iterator6.return();
                }

              case 54:
                _context4.prev = 54;

                if (!_didIteratorError6) {
                  _context4.next = 57;
                  break;
                }

                throw _iteratorError6;

              case 57:
                return _context4.finish(54);

              case 58:
                return _context4.finish(51);

              case 59:
                _iteratorNormalCompletion5 = true;
                _context4.next = 26;
                break;

              case 62:
                _context4.next = 68;
                break;

              case 64:
                _context4.prev = 64;
                _context4.t2 = _context4['catch'](24);
                _didIteratorError5 = true;
                _iteratorError5 = _context4.t2;

              case 68:
                _context4.prev = 68;
                _context4.prev = 69;

                if (!_iteratorNormalCompletion5 && _iterator5.return) {
                  _iterator5.return();
                }

              case 71:
                _context4.prev = 71;

                if (!_didIteratorError5) {
                  _context4.next = 74;
                  break;
                }

                throw _iteratorError5;

              case 74:
                return _context4.finish(71);

              case 75:
                return _context4.finish(68);

              case 76:
              case 'end':
                return _context4.stop();
            }
          }
        }, _callee3, this, [[4, 8, 12, 20], [13,, 15, 19], [24, 64, 68, 76], [34, 47, 51, 59], [52,, 54, 58], [69,, 71, 75]]);
      }));

      function _relativize(_x3, _x4) {
        return _ref3.apply(this, arguments);
      }

      return _relativize;
    }()
  }, {
    key: '_relativizeText',
    value: function _relativizeText(text) {
      return text.replace(/http:\/\/catalog\.prosv\.ru/g, '.');
    }
  }, {
    key: '_linearize',
    value: function _linearize(object, flatArray, level) {
      var _this2 = this;

      if (!level) {
        level = 0;
      }
      var obj = Object.assign({}, object);
      obj.level = level;
      flatArray.push(obj);
      if (object.children) {
        object.children.forEach(function (o) {
          _this2._linearize(o, flatArray, level + 1);
        });
      }
      // delete obj.children;
    }
  }, {
    key: '_processSvg',
    value: function _processSvg(srcPath, destPath) {
      return new Promise(function (resolve, reject) {
        var data = _fs2.default.readFileSync(srcPath, 'utf8');
        var parser = new _xml2js2.default.Parser();
        parser.parseString(data, function (err, xml) {
          if (err) {
            return reject(err);
          }
          xml.svg['$'].xmlns = 'http://www.w3.org/2000/svg';
          var builder = new _xml2js2.default.Builder();
          var xmlString = builder.buildObject(xml);
          _fs2.default.writeFileSync(destPath, xmlString, 'utf8');
          resolve();
        });
      });
    }
  }, {
    key: '_processDownloadedFile',
    value: function _processDownloadedFile(filePath) {
      switch (_path2.default.extname(filePath)) {
        case '.svg':
          return this._processSvg(filePath, filePath);
      }
      return Promise.resolve();
    }
  }, {
    key: '_downloadFile',
    value: function _downloadFile(url, destDir, saveUrlPath, destFileName, maxImageSize) {
      var _this3 = this;

      console.log('Download', url);
      var def = _q2.default.defer();
      var pathToSave = !saveUrlPath ? _path2.default.join(destDir, destFileName || '') : _path2.default.join(destDir, '.' + _url2.default.parse(url).pathname);
      if (this._doNotUpdateResources && (0, _fileExists2.default)(pathToSave)) {
        def.resolve();
      } else {
        var r = (0, _request2.default)(url);
        _mkdirp2.default.sync(_path2.default.parse(pathToSave).dir);
        r.pause();
        r.on('error', function (err) {
          def.reject(new Error(err));
        }).on('response', function (resp) {
          if (resp.statusCode === 200) {
            r.pipe(_fs2.default.createWriteStream(pathToSave)).on('finish', function () {
              if (!maxImageSize) {
                _this3._processDownloadedFile(pathToSave).then(def.resolve).catch(def.reject);
              } else {
                var w = maxImageSize.width;
                var h = maxImageSize.height;
                var cmd = 'convert \'' + pathToSave + '\' -resize ' + w + '\\>x' + h + '\\> \'' + pathToSave + '\'';
                console.log(cmd);
                _easyimage2.default.exec(cmd).then(def.resolve).catch(def.reject);
              }
            });
            r.resume();
          } else {
            def.reject(new Error('Server returned status code ' + resp.statusCode));
          }
        });
      }
      return def.promise;
    }
  }]);

  return ProsvConverter;
}();

exports.default = ProsvConverter;
;