# prosv-converter [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> Converter for content of Prosvescheniye publiser.

## Installation

```sh
$ npm install --save prosv-converter
```

## Usage

```js
var prosvConverter = require('prosv-converter');

prosvConverter('Rainbow');
```
## License

 © [Denis Bezrukov]()


[npm-image]: https://badge.fury.io/js/prosv-converter.svg
[npm-url]: https://npmjs.org/package/prosv-converter
[travis-image]: https://travis-ci.org//prosv-converter.svg?branch=master
[travis-url]: https://travis-ci.org//prosv-converter
[daviddm-image]: https://david-dm.org//prosv-converter.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//prosv-converter
[coveralls-image]: https://coveralls.io/repos//prosv-converter/badge.svg
[coveralls-url]: https://coveralls.io/r//prosv-converter
